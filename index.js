var  express = require('express');
var  app = express();
var  utils = require('./utils')
var bodyParser = require('body-parser')
var cors = require('cors');

app.use(express.static('public'));

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



// deprecated
//app.use(bodyParser());

app.get('/', function (req, res) {
  res.sendfile('./views/index.html')
});

app.post('/ok', function (req, res) {
  utils.getRequest(req.body.issuekey,function(response){
    res.send(response.body)
  });
});



app.listen(3021, function () {
  console.log('Node Server running on port: 3021');
});

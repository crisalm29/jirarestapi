$("#btn_single_search").click(function(){
  $.ajax({
    type:"POST",
    contentType: "application/json",
    dataType: "json",
    //url:"http://localhost:3021/ok",
    url:"http://35.226.120.13:3021/ok",
    data:JSON.stringify({issuekey:$("#input_single_key").val()}),
    success:function(result){
	
	if(typeof result.fields != 'undefined'){ 
      		fields=result.fields
      		//console.log(result)
    		$('#single_search_results').html(`
      <h3><b>Key</b>: ${result.key}</h3>
      <h3><b>Summary</b>: ${fields.summary}</h3>
      <h3><b>Description</b>: ${fields.description}</h3>
      <h3><b>Creator</b>: ${fields.creator.displayName}</h3>  
      <h3><b>Assignee</b>: ${fields.assignee.displayName}</h3>  
      		`)
     }else{
		$('#single_search_results').html('<h3>Issue not found.</h3>');
	}
    },
    error:function(x,status,error){
      console.log(error);


      console.log('35.226.120.13:3021 is not accessible')
    }
  });

});
